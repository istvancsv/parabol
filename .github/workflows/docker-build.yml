name: Build Docker image

on: [push]

env:
  PARABOL_DOCKERFILE: ./docker/parabol-ubi/docker-build/dockerfiles/pipeline.dockerfile
  PARABOL_BUILD_ENV_PATH: docker/parabol-ubi/docker-build/environments/pipeline
  DEPENDENCIES_CACHE: cache-node-modules

jobs:
  build-and-push:
    runs-on: ubuntu-22.04
    permissions:
      contents: "read"
      id-token: "write"
    services:
      postgres:
        image: postgres:12.10-alpine
        # This env variables must be the same in the file PARABOL_BUILD_ENV_PATH
        env:
          POSTGRES_PASSWORD: "temppassword"
          POSTGRES_USER: "tempuser"
          POSTGRES_DB: "tempdb"
        ports:
          - 5432:5432
        # Set health checks to wait until postgres has started
        options: >-
          --health-cmd pg_isready
          --health-interval 10s
          --health-timeout 5s
          --health-retries 5
      rethinkdb:
        image: rethinkdb:2.4.2
        ports:
          - 8080:8080
          - 28015:28015
          - 29015:29015
      redis:
        image: redis:6.2.6
        ports:
          - 6379:6379
    steps:
      - name: Checkout
        uses: actions/checkout@v3

      - name: Set up Docker Buildx
        uses: docker/setup-buildx-action@v2
        with:
          buildkitd-flags: "--allow-insecure-entitlement network.host"
          driver-opts: network=host

      - id: "auth"
        name: "Authenticate to Google Cloud"
        uses: "google-github-actions/auth@v1"
        with:
          token_format: "access_token"
          workload_identity_provider: ${{ secrets.GCP_WI_PROVIDER_NAME }}
          service_account: ${{ secrets.GCP_SA_EMAIL }}

      - uses: "docker/login-action@v2"
        with:
          registry: ${{ secrets.GCP_DOCKER_REGISTRY }}
          username: "oauth2accesstoken"
          password: "${{ steps.auth.outputs.access_token }}"

      # 1.- Only images produced on tags are built with addiotional security. It takes about 2 more minutes to build.
      # 2.- Change the NO_DEPS for tags to true whenenver we want to start building using the --no-deps option. It requires more memory than what is available in the free runners and it also takes more time to build.
      - name: Setup environment variables
        run: |
          NODE_VERSION=$(jq -r -j '.engines.node|ltrimstr("^")' package.json)
          echo "NODE_VERSION=${NODE_VERSION}" >> $GITHUB_ENV

          if [ ${{github.ref_type}} = "tag" ]; then
            echo "NO_DEPS=false" >> $GITHUB_ENV
            echo "BUILD_WITH_SECURITY=true" >> $GITHUB_ENV
            DOCKER_REPOSITORY_FOR_REF=${{ secrets.GCP_AR_PARABOL }}
          else
            echo "NO_DEPS=false" >> $GITHUB_ENV
            echo "BUILD_WITH_SECURITY=true" >> $GITHUB_ENV
            DOCKER_REPOSITORY_FOR_REF=${{ secrets.GCP_AR_PARABOL_DEV }}
          fi

          echo "DOCKER_REPOSITORY_FOR_REF=${DOCKER_REPOSITORY_FOR_REF}" >> $GITHUB_ENV

          GITHUB_REF_NAME_NORMALIZED=$(echo ${{github.ref_name}} | tr / -)
          echo "GITHUB_REF_NAME_NORMALIZED=${GITHUB_REF_NAME_NORMALIZED}" >> $GITHUB_ENV

      - name: Setup Node
        uses: actions/setup-node@v3
        with:
          node-version: "${{ env.NODE_VERSION }}"
          cache: "yarn"

      - name: Get cached node modules
        id: cache
        uses: actions/cache@v3
        with:
          path: |
            **/node_modules
          key: ${{ env.DEPENDENCIES_CACHE }}-${{ hashFiles('yarn.lock') }}

      - name: Yarn install
        if: steps.cache.outputs.cache-hit != 'true'
        run: yarn install --frozen-lockfile --immutable && yarn cache clean

      - name: Build the application
        run: |
          cp ${{ env.PARABOL_BUILD_ENV_PATH }} ./.env && \
          yarn db:migrate && \
          yarn pg:migrate up && \
          yarn pg:build && \
          if [ "$_NO_DEPS" = "true" ]; then \
              yarn build --no-deps && \
              rm -rf node_modules packages scripts; \
          else \
              yarn build; \
          fi && \
          rm -rf .env && \
          rm -rf .circleci .git .github .husky .vscode docs static && \
          rm -rf .gitignore docker-compose.yml nginx.conf.sigil *.html *.md CHECKS COPYING .prettierrc

      - name: Build and push
        uses: docker/build-push-action@v4
        with:
          network: host
          allow: network.host
          file: ${{ env.PARABOL_DOCKERFILE }}
          context: .
          build-args: |
            "_NODE_VERSION=${{ env.NODE_VERSION }}"
            "_SECURITY_ENABLED=${{ env.BUILD_WITH_SECURITY }}"
          push: true
          tags: |
            "${{ secrets.GCP_AR_PARABOL_DEV }}:${{github.sha}}"
            "${{ env.DOCKER_REPOSITORY_FOR_REF }}:${{ env.GITHUB_REF_NAME_NORMALIZED }}"
